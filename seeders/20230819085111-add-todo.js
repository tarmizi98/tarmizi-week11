'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert("Todos",
    [
      {
        To_Do: "Belajar",
        Details: "Belajar javascipt",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        To_Do: "Olaharaga",
        Details: "Sepatu roda",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        To_Do: "Mandi",
        Details: "Mandi sore",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        To_Do: "Makan",
        Details: "Ayam Geprek",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        To_Do: "Tidur",
        Details: "Jam 11.00",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]
    , {})
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Todos", null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
