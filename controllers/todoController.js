const {Todo} = require("../models")

class TodoController {
    static findAll = async (req, res, next) => {
        try {
            const todo = await Todo.findAll()
            console.log(todo)
            res.status(200).json(todo)
        } catch (err) {
            next(err)
        }
    }

    static findOne = async (req, res, next) => {
        try {
            const {id} = req.params;
            console.log(req.params)
            const todo = await Todo.findOne({
                where: {
                    id
                }
            });

            if(!todo) {
                throw {name: "ErrorNotFound"}
            }

            res.status(200).json(todo);
        } catch(err) {
            next(err);
        }
    }

    static create = async (req, res, next) => {
        try {
            const {To_Do, Details} = req.body;

            await Todo.create({
                To_Do,
                Details
            })

            res.status(201).json({message: "Todo created"});
        } catch(err) {
            next(err);
        }
    }

    static update = async (req, res, next) => {
        try {
            const {id} = req.params;
            const {To_Do, Details} = req.body;

            // Todo find
            const foundTodo = await Todo.findOne({
                where: {
                    id
                }
            })

            if(!foundTodo) {
                throw {name: "ErrorNotFound"}
            }

            await foundTodo.update({
                To_Do: To_Do || foundTodo.To_Do,
                Details: Details || foundTodo.Details
            })

            // foundTodo data lama

            await foundTodo.save();

            // foundTodo data terupdate

            res.status(200).json({message: "Todo updated"})
        } catch(err) {
            next(err);
        }
    }

    static destroy = async (req, res, next) => {
        try {
            const {id} = req.params;

            const foundTodo = await Todo.findOne({
                where: {
                    id
                }
            })

            if(!foundTodo) {
                throw {name: "ErrorNotFound"}
            }

            await foundTodo.destroy();

            res.status(200).json({message: "Todo deleted"})
        } catch(err) {  
            next(err);
        }
    }

}

module.exports = TodoController