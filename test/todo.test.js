const app = require("../app")
const request = require("supertest");
const {sequelize} = require("../models");
const {queryInterface} = sequelize;

// HOOKS
beforeAll((done) => {

    queryInterface.bulkInsert("Todos", 
        [
            {
                id: 1,
                To_Do: "Belajar",
                Details: "Belajar javascipt",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 2,
                To_Do: "Olaharaga",
                Details: "Sepatu roda",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 3,
                To_Do: "Mandi",
                Details: "Mandi sore",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 4,
                To_Do: "Makan",
                Details: "Ayam Geprek",
                createdAt: new Date(),
                updatedAt: new Date()
            },
            {
                id: 5,
                To_Do: "Tidur",
                Details: "Jam 11.00",
                createdAt: new Date(),
                updatedAt: new Date()
            }
          ]
    , {})
    .then(_ => {
        done()
    })
    .catch(err => {
        console.log(err);
        done(err);
    })
})

afterAll((done) => {

    queryInterface.bulkDelete("Todos", null, {})
        .then(_ => {
            done();
        })
        .catch(err => {
            console.log(err);
            done(err);
        })
})


describe('Testing Todos', () => {

    it("Findall todos", (done) => {

        request(app)
            .get("/todo")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const {body, status} = response;

                expect(status).toEqual(200);
                expect(body.length).toEqual(5);

                const belajar = body[0];

                expect(belajar.To_Do).toBe("Belajar");
                expect(belajar.Details).toBe("Belajar javascipt")
                done();
            })
            .catch(err => {
                console.log(err);
                done(err);
            })
    })

    it("Find detail Todo", (done) => {

        request(app)
            .get("/todo/1")
            // .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const {body, status} = response;
                expect(status).toEqual(200);
                done()
            })
            .catch(err => {
                console.log(err);
                done(err);
            })
    })

    it("Create todos", (done) => {

        request(app)
            .post("/todo")
            .send({
                To_Do: "Bermain",
                Details: "Mobile Legend",
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .then(response => {
                const {body, status} = response;

                expect(body.message).toEqual("Todo created")
                done();
            })
            .catch(err => {
                console.log(err);
                done(err);
            })
    })

    it("Update todos", (done) => {

        request(app)
            .put("/todo/1")
            .send({
                To_Do: "Belajar",
                Details: "Editing",
            })
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const {body, status} = response;

                expect(body.message).toEqual("Todo updated")
                done();
            })
            .catch(err => {
                console.log(err);
                done(err);
            })
    })

    it("Delete Todos", (done) => {

        request(app)
            .delete("/todo/1")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const {body, status} = response;
                expect(body.message).toEqual("Todo deleted")
                done();
            })
    })
})